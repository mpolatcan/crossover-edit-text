package com.mpolatcan.crossover;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.annotation.RequiresApi;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Layout;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.text.method.KeyListener;
import android.text.style.URLSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.ExtractedTextRequest;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.lang.reflect.Field;
import java.util.Locale;

/**
 * Created by mpolatcan-gyte_cse on 24.08.2016.
 */
public class CrossoverEditText extends RelativeLayout {
    private LayoutInflater inflater;
    private Context context;
    private Drawable placeholderImageContent,
                     placeholderBackground;
    private String placeholderTextContent,
                   digits,
                   imeActionLabel;
    private EditText editText;
    private ImageView placeholderImage;
    private TextView placeholderText,
                     hint,
                     materialHint;
    private View placeholder,
                 editTextLine;
    private Typeface textTypeface,
                     placeholderTypeface;
    private TypedArray attrSet;
    private String hintText;
    private Animation inAnimation,
                      outAnimation;
    private int placeholderType,
                textColor,
                cursorColor,
                lineColor,
                hintTextColor,
                inHintTextColor,
                outHintTextColor,
                placeholderTextColor,
                placeholderVectorTintColor,
                inPlaceholderVectorTintColor,
                outPlaceholderVectorTintColor,
                inPlaceholderTextColor,
                outPlaceholderTextColor,
                inputType,
                autoLinkMask,
                breakStrategy,
                imeOption,
                imeActionId,
                lines,
                maxLength,
                maxLines,
                minLines;
    private float textSize,
                  placeholderTextSize,
                  letterSpacing,
                  lineSpacingExtra,
                  lineSpacingMultiplier,
                  editTextPaddingBottom,
                  editTextPaddingRight,
                  editTextPaddingTop,
                  editTextPaddingLeft,
                  scaleWidthFactor,
                  scaleHeightFactor,
                  dx;
    private boolean isCursorVisible,
                    isSingleLine,
                    selectAllOnFocus,
                    defaultPlaceholderAnimationDisabled,
                    customPlaceholderAnimationsEnabled,
                    hintColorAnimationEnabled,
                    lineRemoved;
    private int TEXT = 0,
                IMAGE = 1,
                TINT_NOT_DEFINED = 0x00000000,
                ATTR_NOT_DEFINED = -1;

    /* ------------------- Input Types -------------------------------------- */
    public static int
        CET_TYPE_CLASS_TEXT = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL,
        CET_TYPE_TEXT_FLAG_CAP_CHARACTERS = InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS,
        CET_TYPE_TEXT_FLAG_CAP_WORDS = InputType.TYPE_TEXT_FLAG_CAP_WORDS,
        CET_TYPE_TEXT_FLAG_CAP_SENTENCES = InputType.TYPE_TEXT_FLAG_CAP_SENTENCES,
        CET_TYPE_TEXT_FLAG_AUTO_CORRECT = InputType.TYPE_TEXT_FLAG_AUTO_CORRECT,
        CET_TYPE_TEXT_FLAG_AUTO_COMPLETE = InputType.TYPE_TEXT_FLAG_AUTO_COMPLETE,
        CET_TYPE_TEXT_FLAG_MULTI_LINE = InputType.TYPE_TEXT_FLAG_MULTI_LINE,
        CET_TYPE_TEXT_FLAG_IME_MULTI_LINE = InputType.TYPE_TEXT_FLAG_IME_MULTI_LINE,
        CET_TYPE_TEXT_FLAG_NO_SUGGESTIONS = InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS,
        CET_TYPE_CLASS_URI = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_URI,
        CET_TYPE_CLASS_EMAIL = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS,
        CET_TYPE_CLASS_EMAIL_SUBJECT = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_SUBJECT,
        CET_TYPE_CLASS_SHORT_MESSAGE = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_SHORT_MESSAGE,
        CET_TYPE_CLASS_LONG_MESSAGE = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_LONG_MESSAGE,
        CET_TYPE_CLASS_PERSON_NAME = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PERSON_NAME,
        CET_TYPE_CLASS_POSTAL_ADDRESS = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_POSTAL_ADDRESS,
        CET_TYPE_CLASS_PASSWORD = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD,
        CET_TYPE_CLASS_VISIBLE_PASSWORD = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD,
        CET_TYPE_CLASS_WEB_EDIT_TEXT = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_WEB_EDIT_TEXT,
        CET_TYPE_CLASS_FILTER = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_FILTER,
        CET_TYPE_CLASS_PHONETIC = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PHONETIC,
        CET_TYPE_CLASS_WEB_EMAIL_ADDRESS = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_WEB_EMAIL_ADDRESS,
        CET_TYPE_CLASS_WEB_PASSWORD = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_WEB_PASSWORD,
        CET_TYPE_CLASS_NUMBER = InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL,
        CET_TYPE_CLASS_NUMBER_SIGNED = InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED,
        CET_TYPE_CLASS_NUMBER_DECIMAL = InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL,
        CET_TYPE_CLASS_NUMBER_PASSWORD = InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD,
        CET_TYPE_CLASS_PHONE = InputType.TYPE_CLASS_PHONE,
        CET_TYPE_CLASS_DATETIME = InputType.TYPE_CLASS_DATETIME | InputType.TYPE_DATETIME_VARIATION_NORMAL,
        CET_TYPE_CLASS_DATE = InputType.TYPE_CLASS_DATETIME | InputType.TYPE_DATETIME_VARIATION_DATE,
        CET_TYPE_CLASS_TIME = InputType.TYPE_CLASS_DATETIME | InputType.TYPE_DATETIME_VARIATION_TIME;
    /* ----------------------------------------------------------------------- */

    /* ------------------------------------ Break Strategies ----------------- */
    public static int
        CET_BREAK_STRATEGY_BALANCED = Layout.BREAK_STRATEGY_BALANCED,
        CET_BREAK_STRATEGY_HIGH_QUALITY = Layout.BREAK_STRATEGY_HIGH_QUALITY,
        CET_BREAK_STRATEGY_LOW_SIMPLE = Layout.BREAK_STRATEGY_SIMPLE;
    /* ----------------------------------------------------------------------- */

    public CrossoverEditText(Context context) {
        super(context);
        this.context = context;
        inflater = LayoutInflater.from(context);
        setSaveEnabled(true);
        init();
    }

    public CrossoverEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        inflater = LayoutInflater.from(context);
        attrSet = context.obtainStyledAttributes(attrs, R.styleable.CrossoverEditText);
        setSaveEnabled(true);
        init();
    }

    public CrossoverEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        inflater = LayoutInflater.from(context);
        attrSet = context.obtainStyledAttributes(attrs, R.styleable.CrossoverEditText);
        setSaveEnabled(true);
        init();
    }

    private void init() {
        inflater.inflate(R.layout.crossover_edit_text_layout, this, true);
        placeholderText = ((TextView) findViewById(R.id.crossover_placeholder_text));
        placeholderImage = ((ImageView) findViewById(R.id.crossover_placeholder_image));
        editText = ((EditText) findViewById(R.id.crossover_edit_text));
        hint = ((TextView) findViewById(R.id.crossover_hint));
        materialHint = ((TextView) findViewById(R.id.crossover_hint_mask));
        editTextLine = findViewById(R.id.crossover_edit_text_line);

        updateIds();

        getAttributes();

        setAttributes();

        measureMetrics();

        editText.setOnFocusChangeListener(defaultFocusBehavior);
    }

    private void updateIds() {
        editText.setId(editText.getId() + getId());
        placeholderText.setId(placeholderText.getId() + getId());
        placeholderImage.setId(placeholderImage.getId() + getId());
        hint.setId(hint.getId() + getId());
        materialHint.setId(materialHint.getId() + getId());
        editTextLine.setId(editTextLine.getId() + getId());

        RelativeLayout.LayoutParams editTextParams =
                ((RelativeLayout.LayoutParams) editText.getLayoutParams());
        RelativeLayout.LayoutParams placeholderTextParams =
                ((RelativeLayout.LayoutParams) placeholderText.getLayoutParams());
        RelativeLayout.LayoutParams placeholderImageParams =
                ((RelativeLayout.LayoutParams) placeholderImage.getLayoutParams());
        RelativeLayout.LayoutParams editTextLineParams =
                ((RelativeLayout.LayoutParams) editTextLine.getLayoutParams());

        editTextParams.addRule(RelativeLayout.BELOW, materialHint.getId());
        editText.setLayoutParams(editTextParams);

        placeholderTextParams.addRule(RelativeLayout.BELOW, materialHint.getId());
        placeholderText.setLayoutParams(placeholderTextParams);

        placeholderImageParams.addRule(RelativeLayout.BELOW, materialHint.getId());
        placeholderImage.setLayoutParams(placeholderImageParams);

        editTextLineParams.addRule(RelativeLayout.BELOW, editText.getId());
        editTextLine.setLayoutParams(editTextLineParams);
    }

    private void measureMetrics() {
        placeholder.measure(0, 0);
        editText.measure(0, 0);
        hint.measure(0, 0);
        materialHint.measure(0, 0);

        /* Calculate text scaling factor to scale text to 13 sp */
        scaleHeightFactor =
                ((hint.getMeasuredHeight() * 100 - materialHint.getMeasuredHeight() * 100)
                        / hint.getMeasuredHeight());
        scaleWidthFactor =
                ((hint.getMeasuredWidth() * 100 - materialHint.getMeasuredWidth() * 100)
                        / hint.getMeasuredWidth());

        dx = (hint.getMeasuredWidth() * ((scaleWidthFactor) / 2)) / 100;
        /* -----------------------------------------------------*/

        if (placeholder.getMeasuredHeight() > editText.getMeasuredHeight()) {
            RelativeLayout.LayoutParams placeholderParams = new RelativeLayout.LayoutParams(
                                                        ViewGroup.LayoutParams.WRAP_CONTENT,
                                                        ViewGroup.LayoutParams.WRAP_CONTENT);
            placeholderParams.addRule(RelativeLayout.CENTER_VERTICAL);
            placeholderParams.addRule(RelativeLayout.BELOW, materialHint.getId());
            placeholderParams.setMargins(0, convertDpToPx(15), 0, 0);
            placeholder.setLayoutParams(placeholderParams);

            RelativeLayout.LayoutParams editTextParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    placeholder.getMeasuredHeight());
            editTextParams.addRule(RelativeLayout.BELOW, materialHint.getId());
            editTextParams.setMargins(0, convertDpToPx(15), 0, 0);
            editText.setLayoutParams(editTextParams);
        } else {
            RelativeLayout.LayoutParams editTextParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            editTextParams.addRule(RelativeLayout.BELOW, materialHint.getId());
            editTextParams.setMargins(0, convertDpToPx(15), 0, 0);
            editText.setLayoutParams(editTextParams);

            RelativeLayout.LayoutParams placeholderParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                    editText.getMeasuredHeight());
            placeholderParams.addRule(RelativeLayout.BELOW, materialHint.getId());
            placeholderParams.setMargins(0, convertDpToPx(15), 0, 0);
            placeholderParams.addRule(RelativeLayout.CENTER_VERTICAL);
            placeholder.setLayoutParams(placeholderParams);
        }

        /* Put views to corresponding positions */
        editText.setTranslationX(placeholder.getMeasuredWidth() + convertDpToPx(10));
        hint.setTranslationX(placeholder.getMeasuredWidth() + convertDpToPx(10));
        hint.setTranslationY(placeholder.getMeasuredHeight());
    }

    private void forwardAnim() {
        AnimatorSet animatorSet = new AnimatorSet();

        ObjectAnimator translationX = ObjectAnimator.ofFloat(hint,
                "translationX", -dx, editText.getX());
        ObjectAnimator translationY = ObjectAnimator.ofFloat(hint,
                "translationY", 0f, editText.getY());
        ObjectAnimator scaleX = ObjectAnimator.ofFloat(hint,
                "scaleX", 1.0f);
        ObjectAnimator scaleY = ObjectAnimator.ofFloat(hint,
                "scaleY", 1.0f);
        ObjectAnimator scaleLine = ObjectAnimator.ofFloat(editTextLine,
                "scaleY", 1.0f);

        animatorSet.playTogether(translationX, translationY, scaleX, scaleY,
                scaleLine);
        animatorSet.setInterpolator(new LinearInterpolator());
        animatorSet.setDuration(150);
        animatorSet.start();

        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                if (hintColorAnimationEnabled) {
                    hint.setTextColor(inHintTextColor);
                }
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                hint.setVisibility(INVISIBLE);
                editText.setHint(hint.getText());
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    private void backwardAnim() {
        AnimatorSet animatorSet = new AnimatorSet();

        ObjectAnimator translationX = ObjectAnimator.ofFloat(hint,
                "translationX", editText.getX(), -dx);
        ObjectAnimator translationY = ObjectAnimator.ofFloat(hint,
                "translationY", editText.getY(), 0f);
        ObjectAnimator scaleX = ObjectAnimator.ofFloat(hint,
                "scaleX", (100 - scaleWidthFactor) / 100);
        ObjectAnimator scaleY = ObjectAnimator.ofFloat(hint,
                "scaleY", (100 - scaleHeightFactor) / 100);
        ObjectAnimator scaleLine = ObjectAnimator.ofFloat(editTextLine,
                "scaleY", -4.0f);

        animatorSet.playTogether(translationX, translationY, scaleX, scaleY,
                scaleLine);
        animatorSet.setInterpolator(new LinearInterpolator());
        animatorSet.setDuration(150);
        animatorSet.start();

        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (hintColorAnimationEnabled) {
                    hint.setTextColor(outHintTextColor);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    private void defaultPlaceholderAnimation(final int color) {
        if (placeholderType == TEXT) {
            ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(),
                    placeholderTextColor, color);
            colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

                @Override
                public void onAnimationUpdate(ValueAnimator animator) {
                    placeholderText.setTextColor((Integer) animator.getAnimatedValue());
                }

            });
            colorAnimation.setDuration(300);
            colorAnimation.start();
            placeholderTextColor = color;
        } else if (placeholderVectorTintColor != ATTR_NOT_DEFINED) {
            ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(),
                    placeholderVectorTintColor, color);
            colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

                @Override
                public void onAnimationUpdate(ValueAnimator animator) {
                    placeholderImage.setColorFilter((Integer) animator.getAnimatedValue());
                }

            });
            colorAnimation.setDuration(300);
            colorAnimation.start();
            placeholderVectorTintColor = color;
        }
    }

    private void customPlaceholderAnimation() {

    }

    private void getAttributes() {
        /* ---- Standard EditText attributes ----*/
        inputType = attrSet.getInteger(R.styleable.CrossoverEditText_inputType, CET_TYPE_CLASS_TEXT);
        autoLinkMask = attrSet.getInteger(R.styleable.CrossoverEditText_autoLink, ATTR_NOT_DEFINED);
        lines = attrSet.getInteger(R.styleable.CrossoverEditText_lines, ATTR_NOT_DEFINED);
        maxLines = attrSet.getInteger(R.styleable.CrossoverEditText_maxLines, ATTR_NOT_DEFINED);
        minLines = attrSet.getInteger(R.styleable.CrossoverEditText_minLines, ATTR_NOT_DEFINED);
        maxLength = attrSet.getInteger(R.styleable.CrossoverEditText_maxLength, ATTR_NOT_DEFINED);
        isSingleLine = attrSet.getBoolean(R.styleable.CrossoverEditText_singleLine, false);
        isCursorVisible = attrSet.getBoolean(R.styleable.CrossoverEditText_cursorVisible, true);
        selectAllOnFocus = attrSet.getBoolean(R.styleable.CrossoverEditText_selectAllOnFocus, false);
        breakStrategy = attrSet.getInteger(R.styleable.CrossoverEditText_breakStrategy, ATTR_NOT_DEFINED);
        digits = attrSet.getString(R.styleable.CrossoverEditText_digits);
        imeOption = attrSet.getInteger(R.styleable.CrossoverEditText_imeOptions, ATTR_NOT_DEFINED);
        imeActionId = attrSet.getInteger(R.styleable.CrossoverEditText_imeActionId, ATTR_NOT_DEFINED);
        imeActionLabel = attrSet.getString(R.styleable.CrossoverEditText_imeActionLabel);
        letterSpacing = attrSet.getFloat(R.styleable.CrossoverEditText_letterSpacing, ATTR_NOT_DEFINED);
        lineSpacingExtra = attrSet.getDimension(R.styleable.CrossoverEditText_lineSpacingExtra, ATTR_NOT_DEFINED);
        lineSpacingMultiplier = attrSet.getFloat(R.styleable.CrossoverEditText_lineSpacingMultiplier,
                                                 1.0f);

        editTextPaddingLeft = attrSet.getDimension(R.styleable.CrossoverEditText_editTextPaddingLeft,
                                                 0.0f);
        editTextPaddingRight = attrSet.getDimension(R.styleable.CrossoverEditText_editTextPaddingRight,
                                                 0.0f);
        editTextPaddingBottom = attrSet.getDimension(R.styleable.CrossoverEditText_editTextPaddingBottom,
                                                 0.0f);
        editTextPaddingTop = attrSet.getDimension(R.styleable.CrossoverEditText_editTextPaddingTop,
                                                 0.0f);
        /*---------------------------------------*/

        /* ---- Placeholder attributes ----- */
        placeholderType = attrSet.getInteger(R.styleable.CrossoverEditText_placeholderType, TEXT);
        placeholderImageContent = attrSet.getDrawable(R.styleable.CrossoverEditText_placeholderImage);
        placeholderTextContent = attrSet.getString(R.styleable.CrossoverEditText_placeholderText);
        placeholderBackground = attrSet.getDrawable(R.styleable.CrossoverEditText_placeholderBackground);
        placeholderTextColor = attrSet.getColor(R.styleable.CrossoverEditText_placeholderTextColor,
                Color.BLACK);
        inPlaceholderTextColor = attrSet.getColor(R.styleable.CrossoverEditText_inPlaceholderTextColor,
                Color.BLACK);
        outPlaceholderTextColor = attrSet.getColor(R.styleable.CrossoverEditText_outPlaceholderTextColor,
                Color.BLACK);
        placeholderVectorTintColor = attrSet.getColor(R.styleable.CrossoverEditText_placeholderVectorTintColor,
                                                      TINT_NOT_DEFINED);
        inPlaceholderVectorTintColor = attrSet.getColor(R.styleable.CrossoverEditText_inPlaceholderVectorTintColor,
                                                        TINT_NOT_DEFINED);
        outPlaceholderVectorTintColor = attrSet.getColor(R.styleable.CrossoverEditText_outPlaceholderVectorTintColor,
                                                        TINT_NOT_DEFINED);
        /* --------------------------------- */

        /* ------ Text, line and hint Colors -------- */
        textColor = attrSet.getColor(R.styleable.CrossoverEditText_textColor, Color.BLACK);
        cursorColor = attrSet.getColor(R.styleable.CrossoverEditText_cursorColor, Color.BLACK);
        hintTextColor = attrSet.getColor(R.styleable.CrossoverEditText_hintColor, Color.BLACK);
        inHintTextColor = attrSet.getColor(R.styleable.CrossoverEditText_inHintColor, Color.BLACK);
        outHintTextColor = attrSet.getColor(R.styleable.CrossoverEditText_outHintColor, Color.BLACK);
        lineColor = attrSet.getColor(R.styleable.CrossoverEditText_lineColor, Color.BLACK);
        /* --------------------- */

        /* ----- Text sizes ------ */
        textSize = attrSet.getDimension(R.styleable.CrossoverEditText_textSize, 13f);
        placeholderTextSize = attrSet.getDimension(R.styleable.CrossoverEditText_placeholderTextSize, 13f);
        /* ----------------------- */

        /* ---- Typefaces ---- */
        if (attrSet.getString(R.styleable.CrossoverEditText_textTypeface) != null) {
            textTypeface = Typeface.createFromAsset(context.getAssets(),
                    attrSet.getString(R.styleable.CrossoverEditText_textTypeface));
        }

        if (attrSet.getString(R.styleable.CrossoverEditText_placeholderTypeface) != null) {
            placeholderTypeface = Typeface.createFromAsset(context.getAssets(),
                    attrSet.getString(R.styleable.CrossoverEditText_placeholderTypeface));
        }
        /* ------------------ */

        /* --------- In and out animations -------- */
        if (attrSet.getResourceId(R.styleable.CrossoverEditText_inAnimation, 0) != 0) {
            inAnimation = AnimationUtils.loadAnimation(context,
                    attrSet.getResourceId(R.styleable.CrossoverEditText_inAnimation, 0));
        }

        if (attrSet.getResourceId(R.styleable.CrossoverEditText_outAnimation, 0) != 0) {
            outAnimation = AnimationUtils.loadAnimation(context,
                    attrSet.getResourceId(R.styleable.CrossoverEditText_outAnimation, 0));
        }
        /* ---------------------------------------- */

        hintText = attrSet.getString(R.styleable.CrossoverEditText_hint);

        customPlaceholderAnimationsEnabled = attrSet.getBoolean(
                R.styleable.CrossoverEditText_enableCustomPlaceholderAnimations, false);
        defaultPlaceholderAnimationDisabled = attrSet.getBoolean(
                R.styleable.CrossoverEditText_disableDefaultPlaceholderAnimation, false);
        hintColorAnimationEnabled = attrSet.getBoolean(
                R.styleable.CrossoverEditText_enableHintColorAnimation, false);
        lineRemoved = attrSet.getBoolean(R.styleable.CrossoverEditText_removeLine, false);
    }

    private void setAttributes() {
        // Disable text placeholder and assign image asset
        if (placeholderType == IMAGE) {
            placeholderText.setVisibility(GONE);

            placeholder = placeholderImage;

            if (placeholderImageContent != null) {
                placeholderImage.setImageDrawable(placeholderImageContent);
            }

            if (placeholderVectorTintColor != TINT_NOT_DEFINED) {
                placeholderImage.setColorFilter(placeholderVectorTintColor);
            }

            if (!defaultPlaceholderAnimationDisabled) {
                placeholderVectorTintColor = inPlaceholderVectorTintColor;
                placeholderImage.setColorFilter(inPlaceholderVectorTintColor);
            } else {
                placeholderImage.setColorFilter(placeholderVectorTintColor);
            }
        } else if (placeholderType == TEXT) { // disable image placeholder and assign text
            placeholderImage.setVisibility(GONE);

            placeholder = placeholderText;

            placeholderText.setTextSize(TypedValue.COMPLEX_UNIT_PX, placeholderTextSize);

            if (!defaultPlaceholderAnimationDisabled) {
                placeholderTextColor = inPlaceholderTextColor;
                placeholderText.setTextColor(inPlaceholderTextColor);
            } else {
                placeholderText.setTextColor(placeholderTextColor);
            }

            if (placeholderTypeface != null) {
                placeholderText.setTypeface(placeholderTypeface);
            }

            if (placeholderTextContent != null) {
                placeholderText.setText(placeholderTextContent);
            }
        }

        if (placeholderBackground != null) {
            if (Build.VERSION.SDK_INT >= 16) {
                placeholder.setBackground(placeholderBackground);
            } else {
                placeholder.setBackgroundDrawable(placeholderBackground);
            }
        }

        // If user doesn't give any hint fill blank
        if (hintText != null) {
            hint.setText(hintText);
            materialHint.setText(hintText);
            editText.setHint(hintText);
        } else {
            hint.setText("      ");
            materialHint.setText("      ");
            editText.setHint("      ");
        }

        editText.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        hint.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);

        if (textTypeface != null) {
            editText.setTypeface(textTypeface);
            hint.setTypeface(textTypeface);
            materialHint.setTypeface(textTypeface);
        }

        if (hintColorAnimationEnabled) {
            hintTextColor = inHintTextColor;
            hint.setTextColor(hintTextColor);
            editText.setHintTextColor(hintTextColor);
        } else {
            hint.setTextColor(hintTextColor);
            editText.setHintTextColor(hintTextColor);
        }

        editText.setTextColor(textColor);

        if (lineRemoved) {
            editTextLine.setVisibility(GONE);
        }

        setCursorDrawableColor(editText, cursorColor);

        editText.setInputType(inputType);

        if (autoLinkMask != ATTR_NOT_DEFINED) {
            editText.setAutoLinkMask(autoLinkMask);
        }

        if (lines != ATTR_NOT_DEFINED) {
            editText.setLines(lines);
        }

        if (maxLines != ATTR_NOT_DEFINED) {
            editText.setMaxLines(maxLines);
        }

        if (minLines != ATTR_NOT_DEFINED) {
            editText.setMinLines(minLines);
        }

        if (maxLength != ATTR_NOT_DEFINED) {
            setMaxLength(maxLength);
        }

        editText.setSingleLine(isSingleLine);

        editText.setCursorVisible(isCursorVisible);

        editText.setSelectAllOnFocus(selectAllOnFocus);

        if (Build.VERSION.SDK_INT == 23) {
            editText.setBreakStrategy(breakStrategy);
        }

        if (digits != null) {
            setDigits(digits);
        }

        if (imeOption != ATTR_NOT_DEFINED) {
            editText.setImeOptions(imeOption);
        }

        if (imeActionId != ATTR_NOT_DEFINED && imeActionLabel != null) {
            editText.setImeActionLabel(imeActionLabel, imeActionId);
        }

        if (letterSpacing != ATTR_NOT_DEFINED) {
            if (Build.VERSION.SDK_INT == 21) {
                editText.setLetterSpacing(letterSpacing);
            }
        }

        if (lineSpacingExtra != ATTR_NOT_DEFINED) {
            editText.setLineSpacing(lineSpacingExtra, lineSpacingMultiplier);
        }

        editText.setPadding(convertDpToPx(editTextPaddingLeft), convertDpToPx(editTextPaddingTop),
                            convertDpToPx(editTextPaddingRight), convertDpToPx(editTextPaddingBottom));
    }

    private void setCursorDrawableColor(EditText editText, int color) {
        try {
            Field fCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            fCursorDrawableRes.setAccessible(true);
            int mCursorDrawableRes = fCursorDrawableRes.getInt(editText);
            Field fEditor = TextView.class.getDeclaredField("mEditor");
            fEditor.setAccessible(true);
            Object editor = fEditor.get(editText);
            Class<?> clazz = editor.getClass();
            Field fCursorDrawable = clazz.getDeclaredField("mCursorDrawable");
            fCursorDrawable.setAccessible(true);
            Drawable[] drawables = new Drawable[2];
            drawables[0] = editText.getContext().getResources().getDrawable(mCursorDrawableRes);
            drawables[1] = editText.getContext().getResources().getDrawable(mCursorDrawableRes);
            drawables[0].setColorFilter(color, PorterDuff.Mode.SRC_IN);
            drawables[1].setColorFilter(color, PorterDuff.Mode.SRC_IN);
            fCursorDrawable.set(editor, drawables);
        } catch (Throwable ignored) {
        }
    }

    /* ------- Methods for hint customization ---------- */
    public void setHint(CharSequence hintText) { // COMPLETED
        hint.setText(hintText);
        materialHint.setText(hintText);

        if (hint.getVisibility() != VISIBLE) {
            editText.setHint(hintText);
        }

        measureMetrics();
    }

    public void setHintColor(int color) { // COMPLETED
        hintTextColor = color;
        hint.setTextColor(hintTextColor);
        editText.setHintTextColor(color);
    }

    public CharSequence getHint() { // COMPLETED
        return hint.getText();
    }

    public int getHintColor() { // COMPLETED
        return hintTextColor;
    }

    /* ------------------------------------------------ */

    /* ----- Methods for EditText customization --------*/
    public void setTextSize(float textSize) { // COMPLETED
        this.textSize = textSize;
        editText.setTextSize(textSize);
        hint.setTextSize(textSize);

        measureMetrics();
    }

    public void setTextTypeface(Typeface typeface) {
        if (typeface != null) {
            textTypeface = typeface;
            editText.setTypeface(textTypeface);
            hint.setTypeface(textTypeface);
            materialHint.setTypeface(textTypeface);
        } else {
            Log.w("FONT-ERROR", "Font can't be found");
        }
    }

    public void setTextColor(int textColor) { // COMPLETED
        this.textColor = textColor;
        editText.setTextColor(textColor);
    }

    public float getTextSize() { // COMPLETED
        return textSize;
    }

    public Typeface getTextTypeface() { // COMPLETED
        return textTypeface;
    }

    public int getTextColor() { // COMPLETED
        return textColor;
    }
    /* ----------------------------------------------- */

    /* ----- Methods for placeholderText customization --- */
    public void setPlaceholderText(CharSequence text) { // COMPLETED
        placeholderText.setText(text);

        measureMetrics();
    }

    public void setPlaceholderTextSize(float placeholderTextSize) { // COMPLETED
        placeholderText.setTextSize(placeholderTextSize);
        this.placeholderTextSize = placeholderTextSize;

        measureMetrics();
    }

    public void setPlaceholderTypeface(Typeface typeface) { // COMPLETED
        if (typeface != null) {
            placeholderTypeface = typeface;
            placeholderText.setTypeface(typeface);
        } else {
            Log.w("FONT-ERROR", "Font is null");
        }
    }

    public void setPlaceholderImageAsset(Drawable drawable) {
        if (drawable != null) {
            placeholderImage.setImageDrawable(drawable);
            placeholderImageContent = drawable;

            measureMetrics();
        } else {
            Log.w("ASSET-ERROR", "Image is null");
        }
    }

    public Drawable getPlaceholderImageAsset() {
        return placeholderImageContent;
    }

    public void setPlaceholderTextColor(int color) { // COMPLETED
        placeholderText.setTextColor(color);
        placeholderTextColor = color;
    }

    public CharSequence getPlaceholderText() { // COMPLETED
        return placeholderText.getText();
    }

    public float getPlaceholderTextSize() { // COMPLETED
        return placeholderTextSize;
    }

    public Typeface getPlaceholderTypeface() { // COMPLETED
        return placeholderTypeface;
    }

    public int getPlaceholderTextColor() { // COMPLETED
        return placeholderTextColor;
    }
    /* ------------------------------------------------ */

    /* ---- Custom animations methods ---- */
    public void setInAnimation(Animation inAnimation) {
        this.inAnimation = inAnimation;
    }

    public void setOutAnimation(Animation outAnimation) {
        this.outAnimation = outAnimation;
    }

    public Animation getInAnimation() { // COMPLETED
        return inAnimation;
    }

    public Animation getOutAnimation() { // COMPLETED
        return outAnimation;
    }
    /* ----------------------------------- */

    public void setCursorColor(int color) { // COMPLETED
        setCursorDrawableColor(editText, color);
    }

    public int getCursorColor() {
        return cursorColor;
    }

    /* ------------------- EditText standard setters ------------------- */
    public void setSingleLine() {
        editText.setSingleLine();
    }

    public void setSingleLine(boolean singleLine) {
        editText.setSingleLine(singleLine);
    }

    public void setCursorVisible(boolean visible) {
        editText.setCursorVisible(visible);
    }

    public void setInputType(int type) {
        editText.setInputType(type);
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    public void setLetterSpacing(float letterSpacing) {
        editText.setLetterSpacing(letterSpacing);
    }

    public void setLineSpacing(float add, float mult) {
        editText.setLineSpacing(add, mult);
    }

    public void setLines(int lines) {
        editText.setLines(lines);
    }

    public void setSelection(int index) {
        editText.setSelection(index);
    }

    public void setSelection(int start, int stop) {
        editText.setSelection(start, stop);
    }

    public void setDigits(String digits) {
        editText.setKeyListener(DigitsKeyListener.getInstance(digits));
    }

    public void setImeOptions(int imeOption) {
        editText.setImeOptions(imeOption);
    }

    public void setMaxLength(int length) {
        editText.setFilters(new InputFilter[] { new InputFilter.LengthFilter(length)});
    }

    public void setImeActionLabel(CharSequence imeActionLabel, int imeActionId) {
        editText.setImeActionLabel(imeActionLabel, imeActionId);
    }

    public void setSelectAllOnFocus(boolean selectAllOnFocus) {
        editText.setSelectAllOnFocus(selectAllOnFocus);
    }

    @RequiresApi(Build.VERSION_CODES.M)
    public void setBreakStrategy(int breakStrategy) {
        editText.setBreakStrategy(breakStrategy);
    }

    public void setAutoLinkMask(int mask) {
        editText.setAutoLinkMask(mask);
    }

    public void setMaxLines(int maxLines) {
        editText.setMaxLines(maxLines);
    }

    public void setMinLines(int minLines) {
        editText.setMinLines(minLines);
    }

    public void setTextIsSelectable(boolean selectable) {
        editText.setTextIsSelectable(selectable);
    }

    public void setFilters(InputFilter[] filters) {
        editText.setFilters(filters);
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void setTextLocale(Locale locale) {
        editText.setTextLocale(locale);
    }

    public void setAllCaps(boolean allCaps) {
        editText.setAllCaps(allCaps);
    }

    public void editTextSetClickable(boolean clickable) {
        editText.setClickable(clickable);
    }

    public void editTextSetFocusable(boolean focusable) {
        editText.setFocusable(focusable);
    }

    public void editTextSetLongClickable(boolean longClickable) {
        editText.setLongClickable(longClickable);
    }

    public void setFocusChangeListener(final OnFocusChangeListener listener) {
        editText.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                listener.onFocusChange(v, hasFocus);
                defaultFocusBehavior.onFocusChange(v, hasFocus);
            }
        });
    }

    public void setKeyListener(KeyListener input) {
        editText.setKeyListener(input);
    }

    public void setExtractedText(ExtractedText text) {
        editText.setExtractedText(text);
    }

    public void append(CharSequence text) {
        editText.append(text);
    }

    public void append(CharSequence text, int start, int end) {
        editText.append(text, start, end);
    }

    public void addTextWatcher(TextWatcher watcher) { // COMPLETED
        editText.addTextChangedListener(watcher);
    }

    public void removeTextWatcher(TextWatcher watcher) { // COMPLETED
        editText.removeTextChangedListener(watcher);
    }

    public void setOnEditorActionListener(TextView.OnEditorActionListener listener) {
        editText.setOnEditorActionListener(listener);
    }

    public void onEditorAction(int actionCode) {
        editText.onEditorAction(actionCode);
    }

    public int length() {
        return editText.length();
    }

    public void setCustomSelectionActionModeCallback(ActionMode.Callback actionModeCallback) {
        editText.setCustomSelectionActionModeCallback(actionModeCallback);
    }

    @RequiresApi(Build.VERSION_CODES.M)
    public void setCustomInsertionActionModeCallback(ActionMode.Callback actionModeCallback) {
        editText.setCustomInsertionActionModeCallback(actionModeCallback);
    }

    public void moveCursorToVisibleOffset() {
        editText.moveCursorToVisibleOffset();
    }

    public void endBatchEdit() {
        editText.endBatchEdit();
    }

    public void startBatchEdit() {
        editText.beginBatchEdit();
    }

    public void onBeginBatchEdit() {
        editText.onEndBatchEdit();
    }

    public void onEndBatchEdit() {
        editText.onEndBatchEdit();
    }

    public void editTextCancelLongPress() {
        editText.cancelLongPress();
    }

    public boolean editTextPerformLongClick() {
        return editText.performLongClick();
    }

    public void editTextSetOnLongClickListener(OnLongClickListener l) {
        editText.setOnLongClickListener(l);
    }

    public void editTextSetOnTouchListener(OnTouchListener l) {
        editText.setOnTouchListener(l);
    }

    public void editTextSetPadding(int left, int top, int right, int bottom) {
        editText.setPadding(left, top, right, bottom);
    }
    /* --------------------------------------------------------------------- */

    /* ------------------- EditText standard getters ----------------------- */
    public Editable getText() { // COMPLETED
        return editText.getText();
    }

    public int getInputType() {
        return editText.getInputType();
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    public boolean isCursorVisible() {
        return editText.isCursorVisible();
    }

    public boolean isTextSelectable() {
        return editText.isTextSelectable();
    }

    public boolean isSuggestionsEnabled() {
        return editText.isSuggestionsEnabled();
    }

    public int getAutoLinkMask() {
        return editText.getAutoLinkMask();
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    public int getMaxLines() {
        return editText.getMaxLines();
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    public int getMinLines() {
        return editText.getMinLines();
    }

    public int getLineCount() {
        return editText.getLineCount();
    }

    @RequiresApi(Build.VERSION_CODES.M)
    public int getBreakStrategy() {
        return editText.getBreakStrategy();
    }

    public int getImeOptions() {
        return editText.getImeOptions();
    }

    public CharSequence getImeActionLabel() {
        return editText.getImeActionLabel();
    }

    public int getImeActionId() {
        return editText.getImeActionId();
    }

    public InputFilter[] getFilters() {
        return editText.getFilters();
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    public float getLineSpacingExtra() {
        return editText.getLineSpacingExtra();
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    public float getLineSpacingMultiplier() {
        return editText.getLineSpacingMultiplier();
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public Locale getTextLocale() {
        return editText.getTextLocale();
    }

    public KeyListener getKeyListener() {
        return editText.getKeyListener();
    }

    public URLSpan[] getUrls() {
        return editText.getUrls();
    }

    public boolean extractText(ExtractedTextRequest request, ExtractedText outText) {
        return editText.extractText(request, outText);
    }

    public int editTextGetPaddingBottom() {
        return editText.getPaddingBottom();
    }

    public int editTextGetPaddingTop() {
        return editText.getPaddingTop();
    }

    public int editTextGetPaddingLeft() {
        return editText.getPaddingLeft();
    }

    public int editTextGetPaddingRight() {
        return editText.getPaddingRight();
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public int editTextGetPaddingStart() {
        return editText.getPaddingStart();
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public int editTextGetPaddingEnd() {
        return editText.getPaddingEnd();
    }

    public boolean editTextIsClickable() {
        return editText.isClickable();
    }

    public boolean editTextIsLongClickable() {
        return editText.isLongClickable();
    }

    public boolean editTextIsFocused() {
        return editText.isFocused();
    }

    public boolean editTextIsFocusable() {
        return editText.isFocusable();
    }
    /* --------------------------------------------------------------------- */


    // TODO Updating animation mechanism
    private OnFocusChangeListener defaultFocusBehavior = new OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (hasFocus) {
                if (editText.length() == 0) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            editText.setHint("");
                            hint.setVisibility(VISIBLE);

                            editTextLine.setBackgroundColor(lineColor);

                            // If custom animations enabled do custom animations else default anim
                            if (placeholderType == TEXT && customPlaceholderAnimationsEnabled) {
                                Log.d("TO-DO", "Custom anim mechanism");
                            } else if (!defaultPlaceholderAnimationDisabled) {
                                if (placeholderType == TEXT) {
                                    defaultPlaceholderAnimation(outPlaceholderTextColor);
                                } else if (placeholderType == IMAGE) {
                                    defaultPlaceholderAnimation(outPlaceholderVectorTintColor);
                                }
                            }

                            backwardAnim();
                        }
                    }, 50);
                }
            } else {
                if (editText.length() == 0) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            editTextLine.setBackgroundColor(Color.DKGRAY);

                            if (placeholderType == TEXT && customPlaceholderAnimationsEnabled) {
                                Log.d("TO-DO", "Custom anim mechanism");
                            } else if (!defaultPlaceholderAnimationDisabled) {
                                if (placeholderType == TEXT) {
                                    defaultPlaceholderAnimation(inPlaceholderTextColor);
                                } else if (placeholderType == IMAGE) {
                                    defaultPlaceholderAnimation(inPlaceholderVectorTintColor);
                                }
                            }

                            forwardAnim();
                        }
                    }, 50);
                }
            }
        }
    };

    private int convertDpToPx(float dpVal) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpVal,
                context.getResources().getDisplayMetrics());
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Log.d("ID", "" + getId());

        Bundle bundle = new Bundle();

        bundle.putParcelable("instanceState" + getId(), super.onSaveInstanceState());

        bundle.putBoolean("isFocused" + getId(), editText.isFocused());

        bundle.putString("currentText" + getId(), editText.getText().toString());

        return bundle;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        if (state instanceof Bundle) {
            final Bundle bundle = ((Bundle) state);

            if (bundle.getBoolean("isFocused" + getId())) {
                editText.setFocusableInTouchMode(true);
                editText.requestFocus();
            } else if (bundle.getString("currentText" + getId()).length() > 0) {
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        editTextLine.setBackgroundColor(lineColor);
                        hint.setVisibility(VISIBLE);
                        hint.setTextColor(outHintTextColor);
                        hint.setScaleX((100 - scaleWidthFactor) / 100);
                        hint.setScaleY((100 - scaleHeightFactor) / 100);
                        hint.setTranslationX(-dx);
                        hint.setTranslationY(0f);
                        editText.setHint("");
                        placeholderImage.setColorFilter(outPlaceholderVectorTintColor);
                        placeholderText.setTextColor(outPlaceholderTextColor);
                        editTextLine.setScaleY(-4.0f);
                        invalidate();
                    }
                });

            }

            editText.setText(bundle.getString("currentText" + getId()));
            state = bundle.getParcelable("instanceState" + getId());
        }

        super.onRestoreInstanceState(state);
    }
}